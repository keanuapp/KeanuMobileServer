package im.keanu.nearby.client;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.net.wifi.WifiInfo;
import android.net.wifi.WifiManager;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;


import com.budiyev.android.codescanner.CodeScanner;
import com.budiyev.android.codescanner.CodeScannerView;
import com.budiyev.android.codescanner.DecodeCallback;
import com.google.android.material.snackbar.Snackbar;
import com.google.zxing.Result;

import java.net.MalformedURLException;
import java.net.URL;

import im.keanu.nearby.client.ui.UIManager;

public class MainActivity extends AppCompatActivity {
    // Globals
    private UIManager uiManager;
    private WebViewHelper webViewHelper;
    private boolean intentHandled = false;
    CodeScanner mCodeScanner;

    private String webAppUrl = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        // Setup Theme
        setTheme(R.style.AppTheme_NoActionBar);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);


        if (TextUtils.isEmpty(webAppUrl)) {

            checkCameraAndInitQR();
        }
        else {
            try {
                initWebView(webAppUrl);
            } catch (MalformedURLException e) {
                e.printStackTrace();
                initQRCodeScanner();
            }
        }

    }

    private void initWebView (String webAppUrl) throws MalformedURLException {

        Uri urlApp = Uri.parse(webAppUrl);

        String ssid = urlApp.getQueryParameter("ssid");
        String cert = urlApp.getQueryParameter("cert");

        if (!TextUtils.isEmpty(ssid))
            connectToWifi(ssid,null, webAppUrl);
        
        findViewById(R.id.webView).setVisibility(View.VISIBLE);

        // Setup Helpers
        uiManager = new UIManager(this, urlApp.getHost());
        webViewHelper = new WebViewHelper(this, uiManager, webAppUrl, urlApp.getHost() + ":" + urlApp.getPort());
        // Setup App
        webViewHelper.setupWebView();
        webViewHelper.loadHome();

    }

    private void connectToWifi (String ssid, String key, String webAppUrl) throws MalformedURLException {

        if (ContextCompat.checkSelfPermission(this, Manifest.permission.CHANGE_WIFI_STATE)
                == PackageManager.PERMISSION_DENIED) {
            ActivityCompat.requestPermissions(this, new String[] {Manifest.permission.CHANGE_WIFI_STATE}, 1002);

        }
        else {
            WifiHelper.connectToSsid(this, ssid, key);

        }

    }

    private void checkCameraAndInitQR () {
        if (ContextCompat.checkSelfPermission(this, Manifest.permission.CAMERA)
                == PackageManager.PERMISSION_DENIED)
        {
            ActivityCompat.requestPermissions(this, new String[] {Manifest.permission.CAMERA}, 1001);
        }
        else {
            initQRCodeScanner();
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions,
                                           int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);

        switch (requestCode) {
            case 1001:
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0 &&
                        grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    // Permission is granted. Continue the action or workflow
                    // in your app.
                    initQRCodeScanner();
                } else {
                    // Explain to the user that the feature is unavailable because
                    // the features requires a permission that the user has denied.
                    // At the same time, respect the user's decision. Don't link to
                    // system settings in an effort to convince the user to change
                    // their decision.
                }
                return;
            case 1002:
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0 &&
                        grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    // Permission is granted. Continue the action or workflow
                    // in your app.
                    //not sure what to do

                    initQRCodeScanner();
                } else {
                    // Explain to the user that the feature is unavailable because
                    // the features requires a permission that the user has denied.
                    // At the same time, respect the user's decision. Don't link to
                    // system settings in an effort to convince the user to change
                    // their decision.
                }
                return;
        }
        // Other 'case' lines to check for other
        // permissions this app might request.
    }


    private void initQRCodeScanner () {
        final CodeScannerView scannerView = findViewById(R.id.scanner_view);
        mCodeScanner = new CodeScanner(this, scannerView);
        mCodeScanner.setDecodeCallback(new DecodeCallback() {
            @Override
            public void onDecoded(@NonNull final Result result) {
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        //Toast.makeText(MainActivity.this, result.getText(), Toast.LENGTH_SHORT).show();
                        mCodeScanner.stopPreview();
                        mCodeScanner.releaseResources();
                        scannerView.setVisibility(View.GONE);
                        try {
                            initWebView(result.getText());
                        } catch (MalformedURLException e) {
                            e.printStackTrace();

                            Snackbar sb = Snackbar.make(scannerView, "Invalid URL try again",Snackbar.LENGTH_LONG);
                            sb.show();

                            scannerView.setVisibility(View.VISIBLE);
                            mCodeScanner.startPreview();

                        }
                    }
                });
            }
        });
        scannerView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mCodeScanner.startPreview();
            }
        });
        mCodeScanner.startPreview();

        Snackbar sb = Snackbar.make(scannerView, R.string.scan_server_code,Snackbar.LENGTH_LONG);
        sb.show();
    }


    @Override
    protected void onPause() {
        if (webViewHelper != null)
            webViewHelper.onPause();
        else if (mCodeScanner != null)
            mCodeScanner.releaseResources();

        super.onPause();
    }

    @Override
    protected void onResume() {

        if (webViewHelper != null) {
            webViewHelper.onResume();
            // retrieve content from cache primarily if not connected
            webViewHelper.forceCacheIfOffline();
        }
        else if (mCodeScanner != null) {

            mCodeScanner.startPreview();
        }
        else
        {
            checkCameraAndInitQR();
        }

        super.onResume();
    }

    // Handle back-press in browser
    @Override
    public void onBackPressed() {
        if (!webViewHelper.goBack()) {
            super.onBackPressed();
        }
    }
}
