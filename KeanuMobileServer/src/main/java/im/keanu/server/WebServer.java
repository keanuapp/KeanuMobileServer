package im.keanu.server;

import android.content.Context;
import android.util.Log;
import android.webkit.MimeTypeMap;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.net.URLConnection;
import java.net.URLEncoder;
import java.security.KeyStore;
import java.security.SecureRandom;
import java.security.cert.CertificateException;
import java.security.cert.X509Certificate;
import java.util.HashMap;
import java.util.Map;

import javax.net.ssl.KeyManagerFactory;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLException;
import javax.net.ssl.SSLServerSocket;
import javax.net.ssl.SSLServerSocketFactory;
import javax.net.ssl.TrustManagerFactory;

import fi.iki.elonen.NanoHTTPD;
import im.keanu.server.security.SelfSignedCertificate;

public class WebServer extends NanoHTTPD {

    private Context mContext;
    private String mHostAddress = null;
    private int mHostPort = -1;
    private boolean mUseTLS = false;

    private String mCACert = null;

    public WebServer(Context context, int port, String hostAddress, int hostPort, boolean useTLS)  {
        super(port);
        mContext = context;
        mUseTLS = useTLS;
        mHostAddress = hostAddress;
        mHostPort = hostPort;
    }

    public void setCACert (String caCert)
    {
        mCACert = caCert;
    }

    public void startServer () throws IOException {

        start(NanoHTTPD.SOCKET_READ_TIMEOUT, false);
    }


    @Override
    public Response serve(IHTTPSession session) {

        String mimeType = "text/plain";
        InputStream resp = null;
        Log.d("WebServer", "local server requesting uri " + session.getUri() );

        try {


            String path = session.getUri();

            if (path.endsWith("config.json"))
            {
                BufferedReader br = new BufferedReader(new InputStreamReader(mContext.getAssets().open("weblite/config.json")));
                StringBuffer sb = new StringBuffer();
                String line = null;

                String scheme = "http://";
                if (mUseTLS)
                    scheme = "https://";
                String dynamicHost = scheme + mHostAddress + ":" + mHostPort;

                if (session.getHeaders().containsKey("host"))
                {
                    dynamicHost = session.getHeaders().get("host");
                    if (dynamicHost.contains(":"))
                        dynamicHost = dynamicHost.split(":")[0];
                    dynamicHost = scheme + dynamicHost + ":" + mHostPort;
                }

                while ((line = br.readLine())!=null)
                {
                    line = line.replace("LOCAL_IP_ADDRESS",dynamicHost);
                    sb.append(line).append("\n");
                }

                return newFixedLengthResponse(Response.Status.OK,"application/json",sb.toString());
            }
            else if (path.endsWith("cacert"))
            {
                return newFixedLengthResponse(Response.Status.OK,"application/x-x509-ca-cert",mCACert);

            }
            else {
                path = "weblite" + path;
                if (path.endsWith("/"))
                    path += "index.html";

                resp = mContext.getAssets().open(path);

                mimeType = URLConnection.guessContentTypeFromName(path);
            }



        } catch(IOException ioe) {
            Log.w("WebServer", ioe.toString());
        }

        return newChunkedResponse(Response.Status.OK, mimeType, resp);
    }

    private String getPostDataString(HashMap<String, String> params) throws UnsupportedEncodingException {
        StringBuilder result = new StringBuilder();
        boolean first = true;
        for(Map.Entry<String, String> entry : params.entrySet()){
            if (first)
                first = false;
            else
                result.append("&");

            result.append(URLEncoder.encode(entry.getKey(), "UTF-8"));
            result.append("=");
            result.append(URLEncoder.encode(entry.getValue(), "UTF-8"));
        }

        return result.toString();
    }

}
