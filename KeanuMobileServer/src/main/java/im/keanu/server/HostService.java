package im.keanu.server;

import android.app.PendingIntent;
import android.app.Service;
import android.content.Intent;
import android.graphics.Color;
import android.os.Build;
import android.os.Environment;
import android.os.IBinder;
import android.security.KeyChain;
import android.util.Log;

import androidx.annotation.RequiresApi;
import androidx.core.app.NotificationCompat;

import org.spongycastle.asn1.x500.X500Name;
import org.spongycastle.asn1.x509.SubjectPublicKeyInfo;
import org.spongycastle.cert.X509CertificateHolder;
import org.spongycastle.cert.X509v1CertificateBuilder;
import org.spongycastle.operator.OperatorCreationException;
import org.spongycastle.operator.jcajce.JcaContentSignerBuilder;
import org.spongycastle.util.encoders.Base64Encoder;

import java.io.IOException;
import java.math.BigInteger;
import java.net.Inet4Address;
import java.net.InetAddress;
import java.net.NetworkInterface;
import java.net.SocketException;
import java.security.KeyManagementException;
import java.security.KeyPair;
import java.security.KeyPairGenerator;
import java.security.KeyStore;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import java.security.UnrecoverableKeyException;
import java.security.cert.CertificateException;
import java.security.cert.X509Certificate;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Enumeration;

import javax.net.ssl.KeyManagerFactory;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLException;
import javax.net.ssl.SSLServerSocketFactory;
import javax.net.ssl.TrustManagerFactory;

import im.keanu.server.security.SelfSignedCertificate;

public class HostService extends Service {

    public final static String ACTION_START="start";
    public final static String ACTION_RESTART="restart";


    @Override
    public IBinder onBind(Intent intent) {
        // TODO: Return the communication channel to the service.
        throw new UnsupportedOperationException("Not yet implemented");
    }

    @Override
    public void onCreate() {
        super.onCreate();


        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            setupNotificationChannel();
        }
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {


        try {
            startForegroundNotify ();

            boolean useHttps = intent.getBooleanExtra("secure",false);

            if (intent.getAction().equals(ACTION_START)) {
                startServers(useHttps);
            }
            else if (intent.getAction().equals(ACTION_RESTART)) {
                stopServers();
                startServers(useHttps);
            }

        } catch (Exception e) {
            e.printStackTrace();
        }

        return super.onStartCommand(intent, flags, startId);
    }


    /**
     * To show a notification on service start
     */
    private final static String channelId = "keanu_nearby_id";
    private final static CharSequence channelName = "Keanu Micro Service";
    private final static String channelDescription= "Updates from KMS";

    @RequiresApi(api = Build.VERSION_CODES.O)
    private void setupNotificationChannel ()
    {
        android.app.NotificationManager manager = (android.app.NotificationManager)getSystemService(NOTIFICATION_SERVICE);
        android.app.NotificationChannel channel;
        channel = new android.app.NotificationChannel(channelId, channelName,
                android.app.NotificationManager.IMPORTANCE_HIGH);
        channel.setDescription(channelDescription);
        channel.setLightColor(Color.BLUE);
        channel.setImportance(android.app.NotificationManager.IMPORTANCE_MIN);
        manager.createNotificationChannel(channel);
    }

    private void startForegroundNotify () {

        Intent toLaunch = new Intent(getApplicationContext(),
                MainActivity.class);

        toLaunch.setAction(Intent.ACTION_MAIN);
        toLaunch.addCategory(Intent.CATEGORY_LAUNCHER);
        toLaunch.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);

        PendingIntent resultPendingIntent =
                PendingIntent.getActivity(
                        this,
                        0,
                        toLaunch,
                        PendingIntent.FLAG_UPDATE_CURRENT|PendingIntent.FLAG_IMMUTABLE
                );

        // In this sample, we'll use the same text for the ticker and the expanded notification
        String text = "Server is running";

        NotificationCompat.Builder mBuilder =
                new NotificationCompat.Builder(this, channelId)
                        .setSmallIcon(R.drawable.ic_notification)
                        .setContentTitle(getString(R.string.app_name))
                        .setContentText(text);

        mBuilder.setPriority(NotificationCompat.PRIORITY_MIN);
        mBuilder.setContentIntent(resultPendingIntent);
        mBuilder.setWhen(System.currentTimeMillis());
        mBuilder.setVisibility(NotificationCompat.VISIBILITY_SECRET);

        startForeground(1, mBuilder.build());
    }

    private DendriteServer ds;
    private PortForwarder pf;
    //private WebForwader wf;
    private WebServer ws;

    private void startServers (boolean useTLS) throws Exception {

        ds = new DendriteServer(this);
        String dendriteHost = "127.0.0.1";
        int dendritePort = ds.getLocalPort();

        int webPort = 8080;
        int appPort = 8081;

     //   wf = new WebForwader(this,appPort,useTLS,dendriteHost,dendritePort);
        pf = new PortForwarder();
        ws = new WebServer(this,webPort,getIpAddress(), appPort, useTLS);

        initSSL(useTLS);

     //   wf.startForwarding();
        ws.startServer();

        pf.start(appPort, dendriteHost, dendritePort);
    }


    private void initSSL(boolean useTLS) throws CertificateException, IOException, KeyStoreException, NoSuchAlgorithmException, UnrecoverableKeyException, KeyManagementException, OperatorCreationException {

        final SelfSignedCertificate ssc = new SelfSignedCertificate(getIpAddress());
        final KeyStore keyStore = KeyStore.getInstance("BKS");
        keyStore.load(null, null);
        keyStore.setKeyEntry("key", ssc.key(), null, new X509Certificate[]{ssc.cert()});
        final KeyManagerFactory kmf = KeyManagerFactory.getInstance("X509");
        kmf.init(keyStore, null);
        final TrustManagerFactory tmf = TrustManagerFactory.getInstance(TrustManagerFactory.getDefaultAlgorithm());
        tmf.init(keyStore);
        final SSLContext context = SSLContext.getInstance("TLSv1.2");
        context.init(kmf.getKeyManagers(), tmf.getTrustManagers(), new SecureRandom());

        SSLServerSocketFactory factory = context.getServerSocketFactory();

        String certCA = ssc.generateCACertv3();

        String[] protocols = {"TLSv1.2"};

        if (useTLS) {
            pf.makeSecure(factory, protocols);
            ws.makeSecure(factory, protocols);
        }

        ws.setCACert(certCA);
    }



    @Override
    public void onDestroy() {
        super.onDestroy();
        stopServers();
    }

    private void stopServers () {
        ds.stopDendrite();

    }

    public static String getIpAddress() {
        String ip = "";
        try {
            Enumeration<NetworkInterface> enumNetworkInterfaces = NetworkInterface
                    .getNetworkInterfaces();
            while (enumNetworkInterfaces.hasMoreElements()) {
                NetworkInterface networkInterface = enumNetworkInterfaces
                        .nextElement();
                Enumeration<InetAddress> enumInetAddress = networkInterface
                        .getInetAddresses();
                while (enumInetAddress.hasMoreElements()) {
                    InetAddress inetAddress = enumInetAddress.nextElement();

                    if (inetAddress.isSiteLocalAddress() && inetAddress instanceof Inet4Address) {
                        ip = inetAddress.getHostAddress();
                        return ip;
                    }
                }
            }

        } catch (SocketException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
            ip += "Something Wrong! " + e.toString() + "\n";
        }
        return ip;
    }

    public static ArrayList<Inet4Address> getAllIpAddress() {
        ArrayList<Inet4Address> ipList = new ArrayList<>();
        try {
            Enumeration<NetworkInterface> enumNetworkInterfaces = NetworkInterface
                    .getNetworkInterfaces();
            while (enumNetworkInterfaces.hasMoreElements()) {
                NetworkInterface networkInterface = enumNetworkInterfaces
                        .nextElement();
                Enumeration<InetAddress> enumInetAddress = networkInterface
                        .getInetAddresses();
                while (enumInetAddress.hasMoreElements()) {
                    InetAddress inetAddress = enumInetAddress.nextElement();

                    if (inetAddress instanceof Inet4Address) {
                        ipList.add((Inet4Address)inetAddress);
                    }
                }
            }

        } catch (SocketException e) {
            Log.e("Host Service","error checking inet address",e);
        }
        return ipList;
    }
}