package im.keanu.server;

import android.content.Context;
import android.content.pm.ApplicationInfo;
import android.util.Log;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.StringBufferInputStream;
import java.net.MalformedURLException;
import java.net.URL;
import java.nio.Buffer;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;

import gobind.DendriteMonolith;

public class DendriteServer {

    private Context mContext;
    private DendriteMonolith mMono;
    public DendriteServer (Context context)
    {

        mContext = context;
        startDendrite();
    }

    public void startDendrite ()
    {
        mMono = new DendriteMonolith();
        mMono.setCacheDirectory(mContext.getCacheDir().getAbsolutePath());
        mMono.setStorageDirectory(mContext.getFilesDir().getAbsolutePath());
        mMono.start();

        Log.d("Dendrite","base url:" + mMono.baseURL());


    }

    public void stopDendrite ()
    {
        mMono.stop();
    }

    public int getLocalPort ()  {
        try {
            URL urlServer = new URL(mMono.baseURL());
            return urlServer.getPort();
        }catch (Exception e)
        {
            return -1;
        }

    }
    public String getHostAddress ()
    {
        return mMono.baseURL();
    }



}
