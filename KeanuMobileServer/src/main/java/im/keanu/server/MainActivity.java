package im.keanu.server;

import static im.keanu.server.HostService.ACTION_RESTART;
import static im.keanu.server.HostService.ACTION_START;
import static im.keanu.server.WifiHelper.getCurrentSsid;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.SwitchCompat;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.net.Uri;
import android.net.wifi.WifiManager;
import android.os.Build;
import android.os.Bundle;
import android.text.TextUtils;
import android.text.format.Formatter;
import android.util.Log;
import android.view.View;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.TextView;


import com.google.zxing.BarcodeFormat;
import com.google.zxing.WriterException;
import com.journeyapps.barcodescanner.BarcodeEncoder;

import java.io.IOException;
import java.net.Inet4Address;
import java.net.InetAddress;
import java.net.NetworkInterface;
import java.net.SocketException;
import java.util.ArrayList;
import java.util.Enumeration;

public class MainActivity extends AppCompatActivity {

    private String mLocalUrl = null;

    private boolean mUseHttp = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        getSupportActionBar().hide();

        final SharedPreferences prefs = getPreferences(Context.MODE_PRIVATE);
        mUseHttp = prefs.getBoolean("secure",false);

        SwitchCompat swSecure = ((SwitchCompat) findViewById(R.id.switchSecure));

        swSecure.setChecked(mUseHttp);

        swSecure.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                mUseHttp = isChecked;
                prefs.edit().putBoolean("secure",mUseHttp).commit();
                restartServer();
            }
        });

        ArrayList<Inet4Address> localIps = HostService.getAllIpAddress();
        int localPort = 8080;
        int matrixPort = 8081;
        String scheme = "http://";

        if (mUseHttp)
            scheme = "https://";

        StringBuffer sb = new StringBuffer();
        sb.append(getString(R.string.web_chat_available)).append("\n\n");

        for (Inet4Address ipa : localIps) {

            if (!ipa.isLoopbackAddress()) {
                StringBuffer sbLink = new StringBuffer();

                sbLink.append(scheme);
                sbLink.append(ipa.getHostAddress());
                sbLink.append(":");
                sbLink.append(localPort);

                if (mLocalUrl == null) {
                    mLocalUrl = sbLink.toString();
                }

                sb.append(sbLink);
                sb.append("\n");
            }
        }

        sb.append("\n");
        sb.append(getString(R.string.on_wifi_network));
        sb.append(' ');
        sb.append(getCurrentSsid(this));

        sb.append("\n");
        sb.append(getString(R.string.matrix_server_at));
        sb.append(' ');
        sb.append(matrixPort);

        ((TextView) findViewById(R.id.message)).setText(sb.toString());

        if (!TextUtils.isEmpty(mLocalUrl)) {

            Bitmap bmQrcode = generateQR(mLocalUrl, 1024);
            ((ImageView) findViewById(R.id.qrcode)).setImageBitmap(bmQrcode);
        }

        startServer();

        if (ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION)
                == PackageManager.PERMISSION_DENIED)
        {
            ActivityCompat.requestPermissions(this, new String[] {Manifest.permission.ACCESS_FINE_LOCATION}, 1001);

        }
    }

    private void startServer ()
    {

        Intent intentService = new Intent(this,HostService.class);
        intentService.putExtra("secure",mUseHttp);
        intentService.setAction(ACTION_START);


        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            startForegroundService(intentService);
        }
        else {
            startService(intentService);
        }

    }

    private void restartServer ()
    {
        Intent intentService = new Intent(this,HostService.class);
        stopService(intentService);
        finish();;

        startActivity(new Intent(this,MainActivity.class));
    }

    public void openBrowser (View view) {

        Intent intentBrowser = new Intent(Intent.ACTION_VIEW);
        intentBrowser.setData(Uri.parse(mLocalUrl));
        startActivity(intentBrowser);
    }

    public void toggleServer (View view) {
        Intent intentService = new Intent(this,HostService.class);
        stopService(intentService);
        finish();;
    }

    public static Bitmap generateQR(String content, int size) {
        Bitmap bitmap = null;
        try {
            BarcodeEncoder barcodeEncoder = new BarcodeEncoder();
            bitmap = barcodeEncoder.encodeBitmap(content,
                    BarcodeFormat.QR_CODE, size, size);
        } catch (WriterException e) {
            Log.e("generateQR()", e.getMessage());
        }
        return bitmap;
    }
}