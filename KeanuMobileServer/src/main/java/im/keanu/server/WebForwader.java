package im.keanu.server;

import android.content.Context;

import org.apache.commons.io.IOUtils;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.UnsupportedEncodingException;
import java.net.URL;
import java.net.URLConnection;
import java.net.URLEncoder;
import java.util.HashMap;
import java.util.Map;

import fi.iki.elonen.NanoHTTPD;

public class WebForwader extends NanoHTTPD {

    private Context mContext;
    private boolean mUseTLS = false;

    private String mRemoteHost = null;
    private int mRemotePort = -1;

    public WebForwader(Context context, int port, boolean useTLS, String forwardHost, int forwardPort) {
        super(port);
        mContext = context;
        mUseTLS = useTLS;
        mRemoteHost = forwardHost;
        mRemotePort = forwardPort;
    }

    public void startForwarding() throws IOException {
        start(NanoHTTPD.SOCKET_READ_TIMEOUT, false);

    }





    @Override
    public Response serve(IHTTPSession session) {

        String mimeType = "text/plain";
        InputStream is = null;

        if (session.getMethod() == Method.GET) {

            try {
                StringBuffer fwdPath = new StringBuffer();
                fwdPath.append("http://"); //dendrite is always http on localhost

                fwdPath.append(mRemoteHost).append(":").append(mRemotePort);
                fwdPath.append(session.getUri());

                URLConnection conn = new URL(fwdPath.toString()).openConnection();
                Map<String, String> headers = session.getHeaders();
                is = conn.getInputStream();
                mimeType = conn.getContentType();

                Response resp = newChunkedResponse(Response.Status.OK, mimeType, is);

                for (String key : headers.keySet()) {
                    resp.addHeader(key, headers.get(key));
                }

                return resp;


            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        else if (session.getMethod() == Method.POST)
        {
            try {
                StringBuffer fwdPath = new StringBuffer();
                fwdPath.append("http://"); //dendrite is always http on localhost

                fwdPath.append(mRemoteHost).append(":").append(mRemotePort);
                fwdPath.append(session.getUri());

                URLConnection conn = new URL(fwdPath.toString()).openConnection();
                conn.setDoOutput(true);

                OutputStream os = conn.getOutputStream();
                IOUtils.copy(session.getInputStream(),os);

                Map<String, String> headers = session.getHeaders();
                is = conn.getInputStream();
                mimeType = conn.getContentType();

                Response resp = newChunkedResponse(Response.Status.OK, mimeType, is);

                for (String key : headers.keySet()) {
                    resp.addHeader(key, headers.get(key));
                }

                return resp;


            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        return null;
    }

    private String getPostDataString(HashMap<String, String> params) throws UnsupportedEncodingException {
        StringBuilder result = new StringBuilder();
        boolean first = true;
        for(Map.Entry<String, String> entry : params.entrySet()){
            if (first)
                first = false;
            else
                result.append("&");

            result.append(URLEncoder.encode(entry.getKey(), "UTF-8"));
            result.append("=");
            result.append(URLEncoder.encode(entry.getValue(), "UTF-8"));
        }

        return result.toString();
    }

}
