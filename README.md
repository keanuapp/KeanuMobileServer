# ABOUT THIS PROJECT

Keanu Mobile Server is an android application that contains and runs a Matrix Dendrite server (https://github.com/matrix-org/dendrite) and offers a Keanu Weblite (https://gitlab.com/keanuapp/keanuapp-weblite) front-end interface.

Currently, the server runs all in HTTP mode, without TLS/SSL to avoid issues with self-signed certificate warnings, and due to the fact that IP addresses are used and not host names.

# BUILDING DENDRITE FOR ANDROID

Use GoMobile with the Dendrite-Pinecone build.

# USING THIS SERVER

This app also includes nanohttpd web server running on port 8080, which serves up the Keanu Weblite Matrix Client. This app is configured dynamically to work with the local dendrite instance on the phone.

For example, if Dendrite is running at http://192.168.1.208:8008 then Weblite will be available at http://192.168.29.208:8080/#/createroom (this link brings you directly to the create room form, and your account is automatically created on the fly). Just replace the "192.168.1.208" sample address, with the wifi IP address of your Android device.

You can also point any other Matrix client that can support HTTP connections to this matrix server using the address displayed on the app.

# WHY DID YOU DO THIS?!?!

The idea of small, portable servers that can host services like chat and file sharing is very interesting to us. The fact that Dendrite cross-compiled to Android so easily, and we were able to get it running on an Android phone with minimal trouble was good motivation to try and package this up in an app like this.

Even though the server is only available via HTTP, this does provide value for users connecting to the phone via hotspot or a trusted local LAN network. You can also connect to this server via a Tor Onion Service hosted in the Orbot: Tor for Android app. This means you can offer a full, anonymous chat service with end-to-end encryption directly from any Android phone.


